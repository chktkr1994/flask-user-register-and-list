from flask import Flask, render_template, request, redirect, url_for, session
from flask_mysqldb import MySQL
import MySQLdb.cursors
import re
  
  
app = Flask(__name__)
  
  
app.secret_key = 'xyzsdfg'
  
app.config['MYSQL_HOST'] = 'sql6.freesqldatabase.com'
app.config['MYSQL_USER'] = 'sql6515359'
app.config['MYSQL_PASSWORD'] = 'guL7r4dupn'
app.config['MYSQL_DB'] = 'sql6515359'
  
mysql = MySQL(app)


@app.route('/')
@app.route('/register', methods =['GET', 'POST'])
def register():
    mesage = ''
    
    if request.method == 'POST' and 'name' in request.form and 'password' in request.form and 'email' in request.form and 'phone' in request.form and 'dob' in request.form :
        userName = request.form['name']
        password = request.form['password']
        email = request.form['email']
        phone = request.form['phone']
        dob = request.form['dob']
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM user WHERE email = % s OR phone = %s', (email,phone, ) )
        account = cursor.fetchone()
        
        if account:
            mesage = 'Account already exists !'
        elif not re.match(r'[^@]+@[^@]+\.[^@]+', email):
            mesage = 'Invalid email address !'
        elif not userName or not password or not email or not phone:
            mesage = 'Please fill out the form !'
        else:
            cursor.execute('INSERT INTO user VALUES (NULL, % s, % s, % s, % s, %s)', (userName, email, phone, dob, password, ))
            mysql.connection.commit()
            mesage = 'You have successfully added !'
    elif request.method == 'POST':
        mesage = 'Please fill out the form !'
    return render_template('register.html', mesage = mesage)
    
@app.route('/user', methods =['GET'])
def user():
    cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
    cursor.execute('SELECT * FROM user ORDER BY userid DESC')
    users = cursor.fetchall()
    return render_template('user.html', users = users)


if __name__ == "__main__":
    app.run(debug=True)